#include <stdio.h>
#include <ftxui/dom/elements.hpp>
#include <ftxui/screen/screen.hpp>
#include <memory>
#include "ftxui/dom/node.hpp"
#include "ftxui/screen/color.hpp"
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif
#include <iostream>
#include <cstdlib>

using namespace ftxui;

int chargingWindow() {
  int gaugeValue = 0;

  for(float i = 0.0f; i < 1; i+= 0.0002f) {
      auto document = vbox({
          filler(),
          hbox({
            filler(),
            text("Loading content..."),
            filler(),
          }),
          hbox({
            filler(),
            border(gauge(i)),
            filler(),
          }),
          filler(),
      }) | border;
    auto screen = Screen::Create(Dimension::Full());
    Render(screen, document);
    screen.Print();
  }
  getchar();
  return 0;
};

int main() {
  auto document = vbox({
    hbox({
      text("Paradisea") | flex,
    }) | hcenter | vcenter,
  }) | border | flex;
  auto screen = Screen::Create(Dimension::Full());
  Render(screen, document);
  screen.Print();
  getchar();
  chargingWindow();
};